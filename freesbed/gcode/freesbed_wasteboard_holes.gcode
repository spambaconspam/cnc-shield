(Exported by FreeCAD)
(Post Processor: grbl_post)
(Output Time:2020-06-17 14:12:11.927689)
(begin preamble)
G17 G90
G21
(begin operation: T1: endmill_3mm)
(Path: T1: endmill_3mm)
(T1: endmill_3mm)
(begin toolchange)
; M6 T1
M3 S0.0000
(finish operation: T1: endmill_3mm)
(begin operation: Pocket_wasteboard_mount_holes)
(Path: Pocket_wasteboard_mount_holes)
(Pocket_wasteboard_mount_holes)
G0 Z5.0000
G0 X140.3484 Y-12.6516
G0 Z3.0000
G1 X140.3484 Y-12.6516 Z-3.0000 F15.00
G2 X140.0000 Y-13.4927 Z-3.0000 I-0.3484 J-0.3484 F15.00
G2 X140.0000 Y-12.5073 Z-3.0000 I0.0000 J0.4927 F15.00
G2 X140.3484 Y-12.6516 Z-3.0000 I0.0000 J-0.4927 F15.00
G1 X140.3484 Y-12.6516 Z-6.0000 F15.00
G2 X140.0000 Y-13.4927 Z-6.0000 I-0.3484 J-0.3484 F15.00
G2 X140.0000 Y-12.5073 Z-6.0000 I0.0000 J0.4927 F15.00
G2 X140.3484 Y-12.6516 Z-6.0000 I0.0000 J-0.4927 F15.00
G1 X140.3484 Y-12.6516 Z-8.0000 F15.00
G2 X140.0000 Y-13.4927 Z-8.0000 I-0.3484 J-0.3484 F15.00
G2 X140.0000 Y-12.5073 Z-8.0000 I0.0000 J0.4927 F15.00
G2 X140.3484 Y-12.6516 Z-8.0000 I0.0000 J-0.4927 F15.00
G0 Z5.0000
(finish operation: Pocket_wasteboard_mount_holes)
(begin postamble)
M5
G17 G90
; M2
