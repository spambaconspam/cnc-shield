EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L symbols:Arduino_UNO_R3 A1
U 1 1 5EDFAE74
P 2100 4550
F 0 "A1" H 2100 5731 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 1700 3250 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 2100 4550 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 2100 4550 50  0001 C CNN
	1    2100 4550
	1    0    0    -1  
$EndComp
$Comp
L symbols:Pololu_Breakout_A4988 A2
U 1 1 5EDFC145
P 4900 2100
F 0 "A2" H 5000 1800 50  0000 C CNN
F 1 "A4988_X" H 5100 1700 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 5175 1350 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 5000 1800 50  0001 C CNN
	1    4900 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 5EE00A4D
P 4400 2700
F 0 "#PWR010" H 4400 2550 50  0001 C CNN
F 1 "+5V" H 4415 2873 50  0000 C CNN
F 2 "" H 4400 2700 50  0001 C CNN
F 3 "" H 4400 2700 50  0001 C CNN
	1    4400 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 2600 4400 2600
Wire Wire Line
	4400 2600 4400 2500
Wire Wire Line
	4400 2400 4500 2400
Wire Wire Line
	4500 2500 4400 2500
Connection ~ 4400 2500
Wire Wire Line
	4400 2500 4400 2400
Wire Wire Line
	4400 2700 4400 2600
Connection ~ 4400 2600
$Comp
L Device:CP C5
U 1 1 5EE02645
P 5350 1250
F 0 "C5" V 5605 1250 50  0000 C CNN
F 1 "100uF" V 5514 1250 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 5388 1100 50  0001 C CNN
F 3 "~" H 5350 1250 50  0001 C CNN
	1    5350 1250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5EE04BF1
P 5600 1250
F 0 "#PWR014" H 5600 1000 50  0001 C CNN
F 1 "GND" V 5605 1122 50  0000 R CNN
F 2 "" H 5600 1250 50  0001 C CNN
F 3 "" H 5600 1250 50  0001 C CNN
	1    5600 1250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5EE04F6B
P 5100 3100
F 0 "#PWR013" H 5100 2850 50  0001 C CNN
F 1 "GND" H 5105 2927 50  0000 C CNN
F 2 "" H 5100 3100 50  0001 C CNN
F 3 "" H 5100 3100 50  0001 C CNN
	1    5100 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2900 5100 3000
Wire Wire Line
	4900 2900 4900 3000
Wire Wire Line
	4900 3000 5100 3000
Connection ~ 5100 3000
Wire Wire Line
	5100 3000 5100 3100
Wire Wire Line
	5550 2000 5400 2000
Wire Wire Line
	5400 2100 5550 2100
Wire Wire Line
	5550 2200 5400 2200
Wire Wire Line
	5400 2300 5550 2300
$Comp
L power:+5V #PWR011
U 1 1 5EE07192
P 4900 1100
F 0 "#PWR011" H 4900 950 50  0001 C CNN
F 1 "+5V" H 4915 1273 50  0000 C CNN
F 2 "" H 4900 1100 50  0001 C CNN
F 3 "" H 4900 1100 50  0001 C CNN
	1    4900 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1100 4900 1400
Wire Wire Line
	4500 1700 4400 1700
Wire Wire Line
	4400 1700 4400 1800
Wire Wire Line
	4400 1800 4500 1800
Text GLabel 4350 2100 0    50   Input ~ 0
X_STEP
Text GLabel 4350 2200 0    50   Input ~ 0
X_DIR
Wire Wire Line
	4350 2200 4500 2200
Wire Wire Line
	4500 2100 4350 2100
Text GLabel 1450 4150 0    50   Output ~ 0
X_STEP
Text GLabel 1450 4450 0    50   Output ~ 0
X_DIR
Wire Wire Line
	1600 4150 1450 4150
Wire Wire Line
	1450 4450 1600 4450
Text GLabel 1450 4250 0    50   Output ~ 0
Y_STEP
Text GLabel 1450 4350 0    50   Output ~ 0
Z_STEP
Wire Wire Line
	1450 4250 1600 4250
Wire Wire Line
	1600 4350 1450 4350
Text GLabel 1450 4550 0    50   Output ~ 0
Y_DIR
Text GLabel 1450 4650 0    50   Output ~ 0
Z_DIR
Wire Wire Line
	1450 4550 1600 4550
Wire Wire Line
	1600 4650 1450 4650
Text GLabel 1450 4750 0    50   Output ~ 0
ENABLE
Wire Wire Line
	1450 4750 1600 4750
Text GLabel 4350 2000 0    50   Input ~ 0
ENABLE
Wire Wire Line
	4350 2000 4500 2000
$Comp
L power:GND #PWR02
U 1 1 5EE10831
P 2200 5850
F 0 "#PWR02" H 2200 5600 50  0001 C CNN
F 1 "GND" H 2205 5677 50  0000 C CNN
F 2 "" H 2200 5850 50  0001 C CNN
F 3 "" H 2200 5850 50  0001 C CNN
	1    2200 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5650 2200 5750
Wire Wire Line
	2100 5650 2100 5750
Wire Wire Line
	2100 5750 2200 5750
Connection ~ 2200 5750
Wire Wire Line
	2200 5750 2200 5850
Wire Wire Line
	2000 5650 2000 5750
Wire Wire Line
	2000 5750 2100 5750
Connection ~ 2100 5750
$Comp
L power:+5V #PWR03
U 1 1 5EE122F7
P 2300 3450
F 0 "#PWR03" H 2300 3300 50  0001 C CNN
F 1 "+5V" H 2315 3623 50  0000 C CNN
F 2 "" H 2300 3450 50  0001 C CNN
F 3 "" H 2300 3450 50  0001 C CNN
	1    2300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3450 2300 3550
$Comp
L cnc-shield-rescue:1825910-6-dk_Tactile-Switches S1
U 1 1 5EE14F27
P 2950 3850
F 0 "S1" H 2950 4197 60  0000 C CNN
F 1 "RESET" H 2950 4091 60  0000 C CNN
F 2 "digikey-footprints:Switch_Tactile_THT_6x6mm" H 3150 4050 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 3150 4150 60  0001 L CNN
F 4 "450-1650-ND" H 3150 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "1825910-6" H 3150 4350 60  0001 L CNN "MPN"
F 6 "Switches" H 3150 4450 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 3150 4550 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 3150 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-alcoswitch-switches/1825910-6/450-1650-ND/1632536" H 3150 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 24V" H 3150 4850 60  0001 L CNN "Description"
F 11 "TE Connectivity ALCOSWITCH Switches" H 3150 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3150 5050 60  0001 L CNN "Status"
	1    2950 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3950 2650 3950
Wire Wire Line
	2750 3750 2650 3750
Wire Wire Line
	2650 3750 2650 3950
Connection ~ 2650 3950
Wire Wire Line
	2650 3950 2750 3950
Wire Wire Line
	3150 3750 3250 3750
Wire Wire Line
	3250 3750 3250 3950
Wire Wire Line
	3250 3950 3150 3950
$Comp
L power:GND #PWR07
U 1 1 5EE17E2F
P 3400 3750
F 0 "#PWR07" H 3400 3500 50  0001 C CNN
F 1 "GND" V 3405 3622 50  0000 R CNN
F 2 "" H 3400 3750 50  0001 C CNN
F 3 "" H 3400 3750 50  0001 C CNN
	1    3400 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 3750 3250 3750
Connection ~ 3250 3750
Text GLabel 1450 5050 0    50   Output ~ 0
SPIN_PWM
Text GLabel 1450 5150 0    50   Output ~ 0
Z_ENDSTOP
Text GLabel 1450 4950 0    50   Output ~ 0
Y_ENDSTOP
Text GLabel 1450 4850 0    50   Output ~ 0
X_ENDSTOP
Wire Wire Line
	1450 4850 1600 4850
Wire Wire Line
	1600 4950 1450 4950
Wire Wire Line
	1450 5050 1600 5050
Wire Wire Line
	1600 5150 1450 5150
Wire Wire Line
	5200 1250 5100 1250
Wire Wire Line
	5100 1250 5100 1400
$Comp
L power:+24V #PWR012
U 1 1 5EE24165
P 5100 1100
F 0 "#PWR012" H 5100 950 50  0001 C CNN
F 1 "+24V" H 5115 1273 50  0000 C CNN
F 2 "" H 5100 1100 50  0001 C CNN
F 3 "" H 5100 1100 50  0001 C CNN
	1    5100 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1100 5100 1250
Connection ~ 5100 1250
Wire Wire Line
	5600 1250 5500 1250
Text Notes 5650 750  0    50   ~ 0
MS1, MS2, MS3 are pulled to +5v to enable\nhighest microstepping settings.
$Comp
L Device:C_Small C1
U 1 1 5EE3C872
P 1950 2550
F 0 "C1" V 2000 2450 50  0000 C CNN
F 1 "100nF" V 2000 2700 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 1950 2550 50  0001 C CNN
F 3 "~" H 1950 2550 50  0001 C CNN
	1    1950 2550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5EE4E865
P 2750 2900
F 0 "#PWR06" H 2750 2650 50  0001 C CNN
F 1 "GND" V 2755 2772 50  0000 R CNN
F 2 "" H 2750 2900 50  0001 C CNN
F 3 "" H 2750 2900 50  0001 C CNN
	1    2750 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 1650 1950 1650
$Comp
L Device:R R2
U 1 1 5EE69DEB
P 2100 1400
F 0 "R2" V 2150 1250 50  0000 C CNN
F 1 "1K" V 2100 1400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2100 1400 50  0001 C CNN
F 3 "~" H 2100 1400 50  0001 C CNN
	1    2100 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5EE6B25D
P 2250 1400
F 0 "R3" V 2300 1250 50  0000 C CNN
F 1 "1K" V 2250 1400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2250 1400 50  0001 C CNN
F 3 "~" H 2250 1400 50  0001 C CNN
	1    2250 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5EE72BA0
P 2100 2550
F 0 "C2" V 2150 2450 50  0000 C CNN
F 1 "100nF" V 2150 2700 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2100 2550 50  0001 C CNN
F 3 "~" H 2100 2550 50  0001 C CNN
	1    2100 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 1550 2100 1850
Wire Wire Line
	2250 1550 2250 2050
Wire Wire Line
	2100 1100 1950 1100
Wire Wire Line
	2100 1100 2100 1250
Wire Wire Line
	1950 1100 1950 1250
Wire Wire Line
	2250 1100 2100 1100
Wire Wire Line
	2250 1100 2250 1250
Connection ~ 2100 1100
Connection ~ 1950 1650
Connection ~ 2100 1850
Wire Wire Line
	2100 1850 1600 1850
Connection ~ 2250 2050
Text GLabel 2800 1650 2    50   Output ~ 0
X_ENDSTOP
Text GLabel 2800 1850 2    50   Output ~ 0
Y_ENDSTOP
Text GLabel 2800 2050 2    50   Output ~ 0
Z_ENDSTOP
$Comp
L Device:C_Small C3
U 1 1 5EE7313C
P 2250 2550
F 0 "C3" V 2300 2450 50  0000 C CNN
F 1 "100nF" V 2300 2700 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2250 2550 50  0001 C CNN
F 3 "~" H 2250 2550 50  0001 C CNN
	1    2250 2550
	-1   0    0    1   
$EndComp
NoConn ~ 2600 5350
NoConn ~ 1600 4050
NoConn ~ 1600 3950
NoConn ~ 2600 4150
NoConn ~ 2600 4350
NoConn ~ 2600 4550
NoConn ~ 2600 4650
NoConn ~ 2600 4750
NoConn ~ 2600 4850
NoConn ~ 2600 4950
NoConn ~ 2600 5050
NoConn ~ 2000 3550
NoConn ~ 2200 3550
Text Notes 1650 1000 0    50   ~ 0
Normal closed limits
Connection ~ 2250 1100
$Comp
L power:+5V #PWR05
U 1 1 5EEE1010
P 2500 1100
F 0 "#PWR05" H 2500 950 50  0001 C CNN
F 1 "+5V" V 2515 1228 50  0000 L CNN
F 2 "" H 2500 1100 50  0001 C CNN
F 3 "" H 2500 1100 50  0001 C CNN
	1    2500 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 2900 2250 2650
Wire Wire Line
	2100 2650 2100 2900
Wire Wire Line
	2100 2900 2250 2900
Connection ~ 2250 2900
Wire Wire Line
	2100 2900 1950 2900
Wire Wire Line
	1950 2900 1950 2650
Connection ~ 2100 2900
Wire Wire Line
	1950 1650 2800 1650
Wire Wire Line
	2100 1850 2800 1850
Wire Wire Line
	2250 2050 2800 2050
Wire Wire Line
	2250 1100 2500 1100
Wire Wire Line
	1950 1550 1950 1650
$Comp
L Device:R R1
U 1 1 5EE4022A
P 1950 1400
F 0 "R1" V 2000 1250 50  0000 C CNN
F 1 "1K" V 1950 1400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 1950 1400 50  0001 C CNN
F 3 "~" H 1950 1400 50  0001 C CNN
	1    1950 1400
	-1   0    0    1   
$EndComp
Connection ~ 1950 2900
Wire Wire Line
	1600 2050 2250 2050
$Comp
L Device:C_Small C4
U 1 1 5EF961E2
P 2400 2550
F 0 "C4" V 2450 2450 50  0000 C CNN
F 1 "100nF" V 2450 2700 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2400 2550 50  0001 C CNN
F 3 "~" H 2400 2550 50  0001 C CNN
	1    2400 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 1650 1950 2450
Wire Wire Line
	2100 1850 2100 2450
Wire Wire Line
	2250 2050 2250 2450
Text GLabel 2750 5250 2    50   Output ~ 0
PROBE
Wire Wire Line
	2600 5250 2750 5250
Text GLabel 2800 2250 2    50   Output ~ 0
PROBE
Wire Wire Line
	2800 2250 2650 2250
$Comp
L Connector:Screw_Terminal_01x04 J6
U 1 1 5EFC5AC3
P 5750 2100
F 0 "J6" H 5830 2092 50  0000 L CNN
F 1 "X_STEPPER_PLUG" H 5830 2001 50  0000 L CNN
F 2 "cnc-shield:GX12-4P" H 5750 2100 50  0001 C CNN
F 3 "~" H 5750 2100 50  0001 C CNN
	1    5750 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D1
U 1 1 5EFD1DC6
P 2650 2400
F 0 "D1" V 2650 2350 50  0000 R CNN
F 1 "LED_PROBE" V 2550 2350 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" V 2650 2400 50  0001 C CNN
F 3 "~" V 2650 2400 50  0001 C CNN
	1    2650 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5EFD29DE
P 2650 2650
F 0 "R4" V 2750 2650 50  0000 C CNN
F 1 "250" V 2650 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2650 2650 50  0001 C CNN
F 3 "~" H 2650 2650 50  0001 C CNN
	1    2650 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 2800 2650 2900
Wire Wire Line
	2250 2900 2400 2900
Wire Wire Line
	2400 2450 2400 2250
Connection ~ 2400 2250
Wire Wire Line
	2400 2650 2400 2900
Connection ~ 2400 2900
Wire Wire Line
	2400 2900 2650 2900
Connection ~ 2650 2900
Wire Wire Line
	2650 2900 2750 2900
Wire Wire Line
	2650 2300 2650 2250
Connection ~ 2650 2250
Wire Wire Line
	2400 2250 2650 2250
Wire Wire Line
	1600 2250 2400 2250
Wire Wire Line
	1600 1750 1650 1750
Wire Wire Line
	1650 1750 1650 1950
Wire Wire Line
	1650 2900 1950 2900
Wire Wire Line
	1600 1950 1650 1950
Connection ~ 1650 1950
Wire Wire Line
	1650 1950 1650 2150
Wire Wire Line
	1600 2150 1650 2150
Connection ~ 1650 2150
Wire Wire Line
	1650 2150 1650 2350
Wire Wire Line
	1600 2350 1650 2350
Connection ~ 1650 2350
Wire Wire Line
	1650 2350 1650 2900
Text Notes 1000 3350 0    50   ~ 0
D11 will output range V0-V5\ndepending on gcode
NoConn ~ 1600 5250
$Comp
L symbols:RELAY-SPDT-JQX-15F K1
U 1 1 5F09B4C5
P 7350 4450
F 0 "K1" H 7350 5010 45  0000 C CNN
F 1 "RELAY_SPINDLE" H 7650 4950 45  0000 C CNN
F 2 "cnc-shield:Relay_THT_G5LE-14" H 7350 4900 20  0001 C CNN
F 3 "" H 7350 4450 50  0001 C CNN
F 4 "COMP-10736" H 7350 4831 60  0000 C CNN "Field4"
	1    7350 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5F0A04AF
P 6750 5250
F 0 "#PWR016" H 6750 5000 50  0001 C CNN
F 1 "GND" H 6755 5077 50  0000 C CNN
F 2 "" H 6750 5250 50  0001 C CNN
F 3 "" H 6750 5250 50  0001 C CNN
	1    6750 5250
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 5F0A5D3E
P 6650 4950
F 0 "Q1" H 6850 5150 50  0000 L CNN
F 1 "BC547" H 6850 5050 50  0000 L CNN
F 2 "cnc-shield:TO-92L_Inline_Wide" H 6850 4875 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 6650 4950 50  0001 L CNN
	1    6650 4950
	1    0    0    -1  
$EndComp
Text GLabel 6050 4950 0    50   Input ~ 0
SPIN_PWM
Wire Wire Line
	6050 4950 6100 4950
$Comp
L power:+5V #PWR015
U 1 1 5F0B0265
P 6750 4050
F 0 "#PWR015" H 6750 3900 50  0001 C CNN
F 1 "+5V" H 6765 4223 50  0000 C CNN
F 2 "" H 6750 4050 50  0001 C CNN
F 3 "" H 6750 4050 50  0001 C CNN
	1    6750 4050
	1    0    0    -1  
$EndComp
Text GLabel 7800 4450 2    50   Output ~ 0
220V_TO_SPINDLE
Wire Wire Line
	7700 4450 7800 4450
Wire Wire Line
	7800 4650 7700 4650
Wire Wire Line
	4250 6550 4350 6550
$Comp
L power:+24V #PWR08
U 1 1 5EE35925
P 4250 6550
F 0 "#PWR08" H 4250 6400 50  0001 C CNN
F 1 "+24V" V 4265 6678 50  0000 L CNN
F 2 "" H 4250 6550 50  0001 C CNN
F 3 "" H 4250 6550 50  0001 C CNN
	1    4250 6550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5EE31715
P 4250 6650
F 0 "#PWR09" H 4250 6400 50  0001 C CNN
F 1 "GND" V 4255 6522 50  0000 R CNN
F 2 "" H 4250 6650 50  0001 C CNN
F 3 "" H 4250 6650 50  0001 C CNN
	1    4250 6650
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x06 J8
U 1 1 5F1593B6
P 9900 4550
F 0 "J8" H 9980 4542 50  0000 L CNN
F 1 "220_IN_OUT" H 9980 4451 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-6-5.08_1x06_P5.08mm_Horizontal" H 9900 4550 50  0001 C CNN
F 3 "~" H 9900 4550 50  0001 C CNN
	1    9900 4550
	1    0    0    -1  
$EndComp
$Comp
L symbols:Pololu_Breakout_A4988 A3
U 1 1 5F187D53
P 7350 2100
F 0 "A3" H 7450 1800 50  0000 C CNN
F 1 "A4988_Y" H 7550 1700 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 7625 1350 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 7450 1800 50  0001 C CNN
	1    7350 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR017
U 1 1 5F187D59
P 6850 2700
F 0 "#PWR017" H 6850 2550 50  0001 C CNN
F 1 "+5V" H 6865 2873 50  0000 C CNN
F 2 "" H 6850 2700 50  0001 C CNN
F 3 "" H 6850 2700 50  0001 C CNN
	1    6850 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	6950 2600 6850 2600
Wire Wire Line
	6850 2600 6850 2500
Wire Wire Line
	6850 2400 6950 2400
Wire Wire Line
	6950 2500 6850 2500
Connection ~ 6850 2500
Wire Wire Line
	6850 2500 6850 2400
Wire Wire Line
	6850 2700 6850 2600
Connection ~ 6850 2600
$Comp
L Device:CP C6
U 1 1 5F187D67
P 7800 1250
F 0 "C6" V 8055 1250 50  0000 C CNN
F 1 "100uF" V 7964 1250 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 7838 1100 50  0001 C CNN
F 3 "~" H 7800 1250 50  0001 C CNN
	1    7800 1250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5F187D6D
P 8050 1250
F 0 "#PWR021" H 8050 1000 50  0001 C CNN
F 1 "GND" V 8055 1122 50  0000 R CNN
F 2 "" H 8050 1250 50  0001 C CNN
F 3 "" H 8050 1250 50  0001 C CNN
	1    8050 1250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5F187D73
P 7550 3100
F 0 "#PWR020" H 7550 2850 50  0001 C CNN
F 1 "GND" H 7555 2927 50  0000 C CNN
F 2 "" H 7550 3100 50  0001 C CNN
F 3 "" H 7550 3100 50  0001 C CNN
	1    7550 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2900 7550 3000
Wire Wire Line
	7350 2900 7350 3000
Wire Wire Line
	7350 3000 7550 3000
Connection ~ 7550 3000
Wire Wire Line
	7550 3000 7550 3100
Wire Wire Line
	8000 2000 7850 2000
Wire Wire Line
	7850 2100 8000 2100
Wire Wire Line
	8000 2200 7850 2200
Wire Wire Line
	7850 2300 8000 2300
$Comp
L power:+5V #PWR018
U 1 1 5F187D82
P 7350 1100
F 0 "#PWR018" H 7350 950 50  0001 C CNN
F 1 "+5V" H 7365 1273 50  0000 C CNN
F 2 "" H 7350 1100 50  0001 C CNN
F 3 "" H 7350 1100 50  0001 C CNN
	1    7350 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1100 7350 1400
Wire Wire Line
	6950 1700 6850 1700
Wire Wire Line
	6850 1700 6850 1800
Wire Wire Line
	6850 1800 6950 1800
Text GLabel 6800 2100 0    50   Input ~ 0
Y_STEP
Text GLabel 6800 2200 0    50   Input ~ 0
Y_DIR
Wire Wire Line
	6800 2200 6950 2200
Wire Wire Line
	6950 2100 6800 2100
Text GLabel 6800 2000 0    50   Input ~ 0
ENABLE
Wire Wire Line
	6800 2000 6950 2000
Wire Wire Line
	7650 1250 7550 1250
Wire Wire Line
	7550 1250 7550 1400
$Comp
L power:+24V #PWR019
U 1 1 5F187D94
P 7550 1100
F 0 "#PWR019" H 7550 950 50  0001 C CNN
F 1 "+24V" H 7565 1273 50  0000 C CNN
F 2 "" H 7550 1100 50  0001 C CNN
F 3 "" H 7550 1100 50  0001 C CNN
	1    7550 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 1100 7550 1250
Connection ~ 7550 1250
Wire Wire Line
	8050 1250 7950 1250
$Comp
L Connector:Screw_Terminal_01x04 J7
U 1 1 5F187D9D
P 8200 2100
F 0 "J7" H 8280 2092 50  0000 L CNN
F 1 "Y_STEPPER_PLUG" H 8280 2001 50  0000 L CNN
F 2 "cnc-shield:GX12-4P" H 8200 2100 50  0001 C CNN
F 3 "~" H 8200 2100 50  0001 C CNN
	1    8200 2100
	1    0    0    -1  
$EndComp
$Comp
L symbols:Pololu_Breakout_A4988 A4
U 1 1 5F190FA8
P 9750 2100
F 0 "A4" H 9850 1800 50  0000 C CNN
F 1 "A4988_Z" H 9950 1700 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 10025 1350 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 9850 1800 50  0001 C CNN
	1    9750 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR022
U 1 1 5F190FAE
P 9250 2700
F 0 "#PWR022" H 9250 2550 50  0001 C CNN
F 1 "+5V" H 9265 2873 50  0000 C CNN
F 2 "" H 9250 2700 50  0001 C CNN
F 3 "" H 9250 2700 50  0001 C CNN
	1    9250 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9350 2600 9250 2600
Wire Wire Line
	9250 2600 9250 2500
Wire Wire Line
	9250 2400 9350 2400
Wire Wire Line
	9350 2500 9250 2500
Connection ~ 9250 2500
Wire Wire Line
	9250 2500 9250 2400
Wire Wire Line
	9250 2700 9250 2600
Connection ~ 9250 2600
$Comp
L Device:CP C7
U 1 1 5F190FBC
P 10200 1250
F 0 "C7" V 10455 1250 50  0000 C CNN
F 1 "100uF" V 10364 1250 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 10238 1100 50  0001 C CNN
F 3 "~" H 10200 1250 50  0001 C CNN
	1    10200 1250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5F190FC2
P 10450 1250
F 0 "#PWR026" H 10450 1000 50  0001 C CNN
F 1 "GND" V 10455 1122 50  0000 R CNN
F 2 "" H 10450 1250 50  0001 C CNN
F 3 "" H 10450 1250 50  0001 C CNN
	1    10450 1250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5F190FC8
P 9950 3100
F 0 "#PWR025" H 9950 2850 50  0001 C CNN
F 1 "GND" H 9955 2927 50  0000 C CNN
F 2 "" H 9950 3100 50  0001 C CNN
F 3 "" H 9950 3100 50  0001 C CNN
	1    9950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 2900 9950 3000
Wire Wire Line
	9750 2900 9750 3000
Wire Wire Line
	9750 3000 9950 3000
Connection ~ 9950 3000
Wire Wire Line
	9950 3000 9950 3100
Wire Wire Line
	10400 2000 10250 2000
Wire Wire Line
	10250 2100 10400 2100
Wire Wire Line
	10400 2200 10250 2200
Wire Wire Line
	10250 2300 10400 2300
$Comp
L power:+5V #PWR023
U 1 1 5F190FD7
P 9750 1100
F 0 "#PWR023" H 9750 950 50  0001 C CNN
F 1 "+5V" H 9765 1273 50  0000 C CNN
F 2 "" H 9750 1100 50  0001 C CNN
F 3 "" H 9750 1100 50  0001 C CNN
	1    9750 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 1100 9750 1400
Wire Wire Line
	9350 1700 9250 1700
Wire Wire Line
	9250 1700 9250 1800
Wire Wire Line
	9250 1800 9350 1800
Text GLabel 9200 2100 0    50   Input ~ 0
Z_STEP
Text GLabel 9200 2200 0    50   Input ~ 0
Z_DIR
Wire Wire Line
	9200 2200 9350 2200
Wire Wire Line
	9350 2100 9200 2100
Text GLabel 9200 2000 0    50   Input ~ 0
ENABLE
Wire Wire Line
	9200 2000 9350 2000
Wire Wire Line
	10050 1250 9950 1250
Wire Wire Line
	9950 1250 9950 1400
$Comp
L power:+24V #PWR024
U 1 1 5F190FE9
P 9950 1100
F 0 "#PWR024" H 9950 950 50  0001 C CNN
F 1 "+24V" H 9965 1273 50  0000 C CNN
F 2 "" H 9950 1100 50  0001 C CNN
F 3 "" H 9950 1100 50  0001 C CNN
	1    9950 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 1100 9950 1250
Connection ~ 9950 1250
Wire Wire Line
	10450 1250 10350 1250
$Comp
L Connector:Screw_Terminal_01x04 J9
U 1 1 5F190FF2
P 10600 2100
F 0 "J9" H 10680 2092 50  0000 L CNN
F 1 "Z_STEPPER_PLUG" H 10680 2001 50  0000 L CNN
F 2 "cnc-shield:GX12-4P" H 10600 2100 50  0001 C CNN
F 3 "~" H 10600 2100 50  0001 C CNN
	1    10600 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5F24B16D
P 6250 4950
F 0 "R5" V 6350 4950 50  0000 C CNN
F 1 "1K" V 6250 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6250 4950 50  0001 C CNN
F 3 "~" H 6250 4950 50  0001 C CNN
	1    6250 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 4650 7000 4650
Wire Wire Line
	7000 4250 6750 4250
Wire Wire Line
	6400 4950 6450 4950
Wire Wire Line
	6750 4050 6750 4250
Wire Wire Line
	6750 4650 6750 4750
Wire Wire Line
	6750 5150 6750 5250
Wire Wire Line
	2350 6800 2350 6500
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5F226A50
P 2350 6800
F 0 "#FLG02" H 2350 6875 50  0001 C CNN
F 1 "PWR_FLAG" H 2350 6973 50  0000 C CNN
F 2 "" H 2350 6800 50  0001 C CNN
F 3 "~" H 2350 6800 50  0001 C CNN
	1    2350 6800
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5F22614D
P 2350 6500
F 0 "#PWR04" H 2350 6250 50  0001 C CNN
F 1 "GND" H 2355 6327 50  0000 C CNN
F 2 "" H 2350 6500 50  0001 C CNN
F 3 "" H 2350 6500 50  0001 C CNN
	1    2350 6500
	-1   0    0    1   
$EndComp
Text GLabel 9400 4350 0    50   Output ~ 0
220V
Wire Wire Line
	9400 4350 9500 4350
Text GLabel 9400 4750 0    50   Input ~ 0
220V_TO_SPINDLE
Wire Wire Line
	9600 4450 9700 4450
Text GLabel 7800 4650 2    50   Input ~ 0
220V
Wire Wire Line
	9700 4650 9600 4650
Wire Wire Line
	9600 4450 9600 4650
Wire Wire Line
	9600 4650 9600 4850
Connection ~ 9600 4650
Wire Wire Line
	9600 4850 9700 4850
Wire Wire Line
	9400 4750 9700 4750
Wire Wire Line
	9500 4350 9500 4550
Wire Wire Line
	9500 4550 9700 4550
Connection ~ 9500 4350
Text Notes 9700 4200 0    50   ~ 0
PIN 1,2 = 220v in, is switched so will switch the rest\n    3,4 = to stepper trafo\n    5,6 = to spindle trafo
Wire Wire Line
	1900 6500 1900 6800
$Comp
L power:+24V #PWR01
U 1 1 5F214F94
P 1900 6500
F 0 "#PWR01" H 1900 6350 50  0001 C CNN
F 1 "+24V" H 1915 6673 50  0000 C CNN
F 2 "" H 1900 6500 50  0001 C CNN
F 3 "" H 1900 6500 50  0001 C CNN
	1    1900 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 6650 4700 6650
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5EFC1A0A
P 4900 6550
F 0 "J5" H 4980 6542 50  0000 L CNN
F 1 "STEPPER_24V_PLUG" H 4980 6451 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2-5.08_1x02_P5.08mm_Horizontal" H 4900 6550 50  0001 C CNN
F 3 "~" H 4900 6550 50  0001 C CNN
	1    4900 6550
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F21480A
P 1900 6800
F 0 "#FLG01" H 1900 6875 50  0001 C CNN
F 1 "PWR_FLAG" H 1900 6973 50  0000 C CNN
F 2 "" H 1900 6800 50  0001 C CNN
F 3 "~" H 1900 6800 50  0001 C CNN
	1    1900 6800
	-1   0    0    1   
$EndComp
Wire Wire Line
	9500 4350 9700 4350
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5EE150EB
P 1400 1650
F 0 "J1" H 1400 1750 50  0000 C CNN
F 1 "X_ENDSTOP_PLUG" H 1850 1600 50  0000 C CNN
F 2 "cnc-shield:GX12-2P" H 1400 1650 50  0001 C CNN
F 3 "~" H 1400 1650 50  0001 C CNN
	1    1400 1650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5EE1584F
P 1400 1850
F 0 "J2" H 1550 1900 50  0000 C CNN
F 1 "Y_ENDSTOP_PLUG" H 1850 1800 50  0000 C CNN
F 2 "cnc-shield:GX12-2P" H 1400 1850 50  0001 C CNN
F 3 "~" H 1400 1850 50  0001 C CNN
	1    1400 1850
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5EE15B0D
P 1400 2050
F 0 "J3" H 1550 1900 50  0000 C CNN
F 1 "Z_ENDSTOP_PLUG" H 1850 2000 50  0000 C CNN
F 2 "cnc-shield:GX12-2P" H 1400 2050 50  0001 C CNN
F 3 "~" H 1400 2050 50  0001 C CNN
	1    1400 2050
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5EE15DE3
P 1400 2250
F 0 "J4" H 1400 2050 50  0000 C CNN
F 1 "PROBE_PLUG" H 1750 2200 50  0000 C CNN
F 2 "cnc-shield:GX12-2P" H 1400 2250 50  0001 C CNN
F 3 "~" H 1400 2250 50  0001 C CNN
	1    1400 2250
	-1   0    0    -1  
$EndComp
NoConn ~ 7700 4250
$Comp
L Diode:1N4001 D2
U 1 1 5EE308DE
P 4500 6550
F 0 "D2" H 4500 6766 50  0000 C CNN
F 1 "1N4001" H 4500 6675 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4500 6375 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 4500 6550 50  0001 C CNN
	1    4500 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6550 4700 6550
$Comp
L Diode:1N4001 D3
U 1 1 5EE4AB8A
P 6750 4450
F 0 "D3" V 6704 4529 50  0000 L CNN
F 1 "1N4001" V 6795 4529 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 6750 4275 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 6750 4450 50  0001 C CNN
	1    6750 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 4650 6750 4600
Connection ~ 6750 4650
Wire Wire Line
	6750 4300 6750 4250
Connection ~ 6750 4250
$EndSCHEMATC
